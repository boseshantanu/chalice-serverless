#Script to install Chalice & Python Dependencies
echo "Installing Chalice & other Project requirements...."
pip install -r ./push-notification/requirements.txt --upgrade --user
pip install -r ./push_notify/requirements.txt --upgrade --user
pip install -r ./push-schedular/requirements.txt --upgrade --user
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -eq "0" ]; then 
	echo "Chalice & project dependancies installed successfully"
    else echo "Check Your requirements.txt for chalice & project dependancies"
fi