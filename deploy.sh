#Deploy Script to execute chalice deploy
function deploy {
  (
    cd $1;\
    echo "Deploying $1 to stage $2 ..."
    chalice deploy --stage $2;\
  )
}
deploy ./push-notification/ $1
deploy ./push_notify/ $1
deploy ./push-schedular/ $1